<?php
require('constants.php');

// Create connection
$connection = mysqli_connect($servername, $username, $password, $db);

// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
} 

echo "Connected successfully";

?>