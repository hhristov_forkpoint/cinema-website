<?php 
	require 'includes/header.php'; 
	$result = $connection->query($theatresQuery);  
?>
	<div class="container-fluid movies-form">
		<div class="row">
		  <div class="col-lg">
		  	<form action="../core/_theatres.php" method="POST">
			  <div class="form-group">
			    <label for="formGroupExampleInput">Name</label>
			    <input type="text" class="form-control" id="t_name" name="t_name">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Address</label>
			    <input type="text" class="form-control" id="t_address" name="t_address">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput">Category</label>
			    <input type="text" class="form-control" id="t_category" name="t_category">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Manager</label>
			    <input type="text" class="form-control" id="t_manager" name="t_manager">
			  </div>
			  <div class="form-group">
				<input type="submit" class="btn btn-primary"/>
			  </div>
			</form>
			</div>
		</div>
		  <div class="col-lg">
		  	<table class="table">
			  	<thead>
				    <tr>
						<th>#</th>
						<th>Name</th>
						<th>Address</th>
						<th>Category</th>
						<th>Manager</th>
				    </tr>
			  	</thead>
			  	<tbody>
				  	<?php $i = 1; while($movie = mysqli_fetch_assoc($result)): ?>
					    <tr>
					      <th scope="row"><?=$i++; ?></th>
					      <td><?=$movie['name']?></td>
					      <td><?=$movie['address']?></td>
					      <td><?=$movie['category']?></td>
					      <td><?=$movie['manager']?></td>
					    </tr>
				    <?php endwhile; ?>
				</tbody>
			</table>
		  </div>
		</div>
	</div>
<?php require 'includes/footer.php'; ?>