<?php 
	require 'includes/header.php'; 
	$movies   = $connection->query($moviesQuery);
	$theatres = $connection->query($theatresQuery);
	$result   = $connection->query($projectionsQuery);
?>
	<div class="container-fluid">
		<div class="row movies-form">
		  <div class="col-lg">
		  	<form action="../core/_projections.php" method="POST">
			  <div class="form-group">
			    <label for="formGroupExampleInput">Movie</label>
			    <select class="form-control" name="p_movie">
			    	<?php while ($movie = mysqli_fetch_assoc($movies)): ?>
			    	<option value="<?=$movie['movien']?>">
			    		<?=$movie['name']?>
			    	</option>
					<?php endwhile; ?>
			    </select>
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Theatre</label>
			    <select class="form-control" name="p_theatre">
			    	<?php while ($theatre = mysqli_fetch_assoc($theatres)): ?>
			    	<option value="<?=$theatre['theatern']?>">
			    		<?=$theatre['name']?>
			    	</option>
					<?php endwhile; ?>
			    </select>
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Date</label>
			    <input class="form-control" type="date" name="p_date">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Time</label>
			    <input class="form-control" type="time" name="p_time">
			  </div>
			  <div class="form-group">
			    <label for="formGroupExampleInput2">Price</label>
			    <input type="text" class="form-control" name="p_price">
			  </div>
			  <div class="form-group">
				<input type="submit" class="btn btn-primary"/>
			  </div>
			</form>
			</div>
		</div>
		  <div class="col-lg">
		  	<table class="table">
			  <thead>
			    <tr>
			      <th>#</th>
			      <th>Movie</th>
			      <th>Theathre</th>
			      <th>Date</th>
			      <th>Time</th>
			      <th>Price</th>
			    </tr>
			  </thead>
			  <tbody>
				<?php $i = 1; while($projection = mysqli_fetch_assoc($result)): ?>
					<tr>
					  <th scope="row"><?=$i++; ?></th>
					  <td><?=dbWhereClause('movies', $projection['movien'])['name']?></td>
					  <td><?=dbWhereClause('theaters', $projection['theatern'])['name']?></td>
					  <td><?=$projection['date']?></td>
					  <td><?=$projection['time']?></td>
					  <td><?=$projection['price']?></td>
					</tr>
				<?php endwhile; ?>
			  </tbody>
			</table>
		  </div>
		</div>
	</div>
<?php require 'includes/footer.php'; ?>