<?php require('../core/init.php') ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Movie theatre</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/index.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
</head>
<body>
<nav class="navbar fixed-top navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="./index.php">Movie theatre</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="./theatres.php">Theatres</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./movies.php">Movies</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./projections.php">Projections</a>
      </li>    
    </ul>
  </div>  
</nav>
<br>


