<?php require 'includes/header.php'; ?>
	<div class="container web-wrapper">
		<div class="row main-thumbnail">
		  <div class="col-lg-6">
		  	<img class="img-fluid" src="../img/avengers.png" alt="">
		  </div>
		  <div class="col-lg-6">
		  	<img class="img-fluid" src="../img/batman.png" alt="">
		  </div>
		</div>

		<!-- Four columns: 25% width on large and up -->
		<div class="row secondary-thumbnail">
		  <div class="col-lg">
		  	<img class="img-fluid" src="../img/inception.png" alt="">
		  </div>
		   <div class="col-lg">
		  	<img class="img-fluid" src="../img/antman.png" alt="">
		  </div>
		   <div class="col-lg">
		  	<img class="img-fluid" src="../img/thor.png" alt="">
		  </div>
		   <div class="col-lg">
		  	<img class="img-fluid" src="../img/hobbit.png" alt="">
		  </div>
		</div>

		<div class="text-center row one-third">
			<div class="col-lg">
				<i class="fa fa-user fa-4x" aria-hidden="true"></i>
				<h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti ad earum hic perspiciatis sit nam laboriosam animi ipsum nulla quia tempora facilis a nesciunt aspernatur non, eum iusto voluptatibus at!</h5>
			</div>
			<div class="col-lg">
				<i class="fa fa-film fa-4x" aria-hidden="true"></i>
				<h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, nihil, iure. Autem ducimus reiciendis nihil aperiam, ipsam magnam odio perspiciatis, eum dolor rem ex neque facilis pariatur unde mollitia minima!</h5>
			</div>
			<div class="col-lg">
				<i class="fa fa-phone-square fa-4x" aria-hidden="true"></i>
				<h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis hic non error impedit quia accusantium iure velit sunt voluptatem dicta, cupiditate aperiam inventore quis deserunt, nobis facilis ipsa pariatur dolores!</h5>
			</div>
		</div>
	
	</div>
<?php require 'includes/footer.php'; ?>