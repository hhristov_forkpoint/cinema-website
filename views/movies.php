<?php 
	require 'includes/header.php'; 
	$result = $connection->query($moviesQuery);  
?>
	<div class="container">
		<div class="row movies-form">
		  <div class="col-lg">
		  	<form action="../core/_movies.php" method="POST">
				<div class="form-group">
					<label for="m_name">Name</label>
					<input type="text" class="form-control" name="m_name" id="m_name">
				</div>
				<div class="form-group">
					<label for="m_category">Category</label>
					<input type="text" class="form-control" name="m_category" id="m_category">
				</div>
				<div class="form-group">
					<label for="m_director">Director</label>
					<input type="text" class="form-control" name="m_director" id="m_director">
				</div>
				<div class="form-group">
					<label for="m_writer">Writer</label>
					<input type="text" class="form-control" name="m_writer" id="m_writer">
				</div>
				<div class="form-group">
					<label for="m_composer">Composer</label>
					<input type="text" class="form-control" name="m_composer" id="m_composer">
				</div>
				<div class="form-group">
					<label for="m_theme">Theme</label>
					<input type="text" class="form-control" name="m_theme" id="m_theme">
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-primary"/>
				</div>
			</form>
		   </div>
		</div>
		  <div class="col-lg">
		  	<table class="table">
			  <thead>
			    <tr>
			      <th>#</th>
			      <th>Name</th>
			      <th>Category</th>
			      <th>Director</th>
			      <th>Writer</th>
			      <th>Composer</th>
			      <th>Theme</th>
			    </tr>
			  </thead>
			  	<tbody>
				  	<?php $i = 1; while($movie = mysqli_fetch_assoc($result)): ?>
					    <tr>
					      <th scope="row"><?=$i++;?></th>
					      <td><?=$movie['name']?></td>
					      <td><?=$movie['category']?></td>
					      <td><?=$movie['director']?></td>
					      <td><?=$movie['writer']?></td>
					      <td><?=$movie['composer']?></td>
					      <td><?=$movie['theme']?></td>
					    </tr>
				    <?php endwhile; ?>
				</tbody>
			</table>
		  </div>
		</div>
	</div>
<?php require 'includes/footer.php'; ?>